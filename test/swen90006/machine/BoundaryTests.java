package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
  
  //------My test cases--------------
  @Test (expected = Exception.class)
  public void B1() {
	  final List<String> lines = new ArrayList();
	  lines.add("");
	  Machine m = new Machine();
	  m.execute(lines);
  }
  
  @Test (expected = InvalidInstructionException.class)
  public void B2() {
	  final List<String> lines = new ArrayList();
	  lines.add("aaaa");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B3() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R0 0");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B4() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R-1 0");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B5() {
	  final List<String> lines = new ArrayList();
	  lines.add("ADD R31 R1 R2");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B6() {
	  final List<String> lines = new ArrayList();
	  lines.add("ADD R32 R1 R2");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B7() {
	  final List<String> lines = new ArrayList();
	  lines.add("LDR R1 R0 -65535");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B8() {
	  final List<String> lines = new ArrayList();
	  lines.add("LDR R1 R0 -65536");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B9() {
	  final List<String> lines = new ArrayList();
	  lines.add("STR R0 65535 R1");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B10() {
	  final List<String> lines = new ArrayList();
	  lines.add("STR R0 65536 R1");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test 
  public void B11() {
	  final List<String> lines = new ArrayList();
	  lines.add("RET R1");
	  Machine m = new Machine();
	  assertEquals(0, m.execute(lines));
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B12() {
	  final List<String> lines = new ArrayList();
	  lines.add("JZ R1 -1");
	  Machine m = new Machine();
	  m.execute(lines);
  } 
  
  @Test (expected = InvalidInstructionException.class)
  public void B13() {
	  final List<String> lines = new ArrayList();
	  lines.add("MUL R2 R1 R0");
	  lines.add("XXX XX XX XX");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test 
  public void B14() {
	  final List<String> lines = new ArrayList();
	  lines.add("div r0 R1 R2");
	  lines.add("RET R0");
	  Machine m = new Machine();
	  assertEquals(0, m.execute(lines));
	  
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B15() {
	  final List<String> lines = new ArrayList();
	  lines.add("DIV R0 R1 R2");
	  lines.add("RET R-1");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B16() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R1 0");
	  lines.add("JZ R31 4");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B17() {
	  final List<String> lines = new ArrayList();
	  Machine m = new Machine();
	  lines.add("MOV R1 0");
	  lines.add("JZ R32 4");
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B18() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R1 0");
	  lines.add("JMP -65535");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B19() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R1 0");
	  lines.add("JMP -65537");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B20() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R1 -3");
	  lines.add("JZ R1 4");
	  lines.add("MOV R1 65535");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = InvalidInstructionException.class)
  public void B21() {
	  final List<String> lines = new ArrayList();
	  lines.add("MOV R1 -3");
	  lines.add("JZ R1 4");
	  lines.add("MOV R1 65536");
	  Machine m = new Machine();
	  m.execute(lines);
  }  
  
  @Test (expected = NoReturnValueException.class)
  public void B22() {
	  final List<String> lines = new ArrayList();
	  Machine m = new Machine();
	  lines.add("");
	  lines.add("SUB R2 R1 R0");
	  lines.add("JMP 3");
	  lines.add("RET R2");
	  m.execute(lines);
  }  
  
  @Test 
  public void B23() {
	  final List<String> lines = new ArrayList();
	  Machine m = new Machine();
	  lines.add("MOV R1 -1");
	  lines.add("RET R1");
	  assertEquals(-1, m.execute(lines));
  }
  
}
